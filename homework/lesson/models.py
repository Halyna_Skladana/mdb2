# from djongo import models
# # from django.db import models
#
# class AuthorBook(models.Model):
#     author = models.CharField(max_length=200, null=True)

from django.db import models


# Create your models here.
class Employee(models.Model):
    employee_id = models.CharField(max_length=20)
    employee_name = models.CharField(max_length=20)
    employee_title = models.CharField(max_length=10)
